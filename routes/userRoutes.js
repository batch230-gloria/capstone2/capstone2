const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");


router.post("/register", userControllers.registerUser);

router.post("/login", userControllers.loginUser);

router.get("/details", auth.verify, userControllers.getUserProfile);

router.patch("/:userId/updateUser", userControllers.updateUserToAdmin); 

router.get("/orders", auth.verify, userControllers.getUserOrders);

router.get("/allOrders", auth.verify, userControllers.getAllOrders);

router.get("/allUsers", auth.verify, userControllers.getAllUserDetails);

module.exports = router;
