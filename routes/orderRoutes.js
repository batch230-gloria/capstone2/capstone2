const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers.js");
const auth = require("../auth.js");

router.post("/:productId/makeOrder", auth.verify, orderControllers.addOrder);

/*router.post("/addToCart", orderControllers.addToCart);*/

module.exports = router;