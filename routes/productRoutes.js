const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");


router.post("/add", auth.verify, productControllers.addProduct);

router.get("/active", productControllers.getAllActiveProduct);

router.get("/:productId", productControllers.getProduct);

router.patch("/:productId/update", auth.verify, productControllers.updateProduct);

router.patch("/:productId/archive", auth.verify, productControllers.archiveProduct);

module.exports = router;