const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	
	name:{
		type: String,
		required: [true, "Product is required"]
	},

	description: {
		type: String,
		required: [true, "Description is required"]
	},

	price:{
		type: Number,
		required: [true, "Price is required"]
	},

	stocks:{
		type: Number,
		required: [true, "Slots is required"]
	},

	isActive:{
		type: Boolean,
		default: true
	},

	createdOn:{
			type: Date,
			default: new Date()
	}

	// [STRETCH GOAL]
	/*orders:[{
		quantity:{
			type: Number
		},
		subtotal:{
			type: Number
		}	
	}]*/
	
});


module.exports = mongoose.model("Product", productSchema);

