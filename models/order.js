const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	

	userId:{
		type: "String"
	},
	email:{
		type: "String"
	},
	quantity:{
		type: Number,
		required: [true, "quantity is required"]
	},
	totalAmount:{
		type: "String"
	},
	productId:{
		type: "String"
	},
	purchasedOn:{
		type: Date,
		default: new Date()
	}

});


module.exports = mongoose.model("Order", orderSchema);