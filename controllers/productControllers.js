const mongoose = require("mongoose");
const User = require("../models/user.js");
const Product = require("../models/product");
const Order = require("../models/order.js");
const auth = require("../auth");

// Create product (Admin Only)
module.exports.addProduct = (req, res) => {

	const productData = auth.decode(req.headers.authorization);

	let newProduct = new Product({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price,
		stocks: req.body.stocks
	});

	if(productData.isAdmin == true){

		return newProduct.save()
		.then(course => {
			console.log(course);
			res.send("Product Successfully Created!")
		})
		.catch(error => {
			console.log(error);
			res.send("Product Creation Failed!");
		});
	}
	else {
		return res.status(404).send("Validation failed, admin permission needed!");
	};

};


// Retrieve all active products
module.exports.getAllActiveProduct = (req, res) =>{
	return Product.find({isActive: true},{__v: 0, createdOn: 0, description: 0}).then(result => res.send(result)).catch(error =>res.send(false));
}


// Retrieve single product
module.exports.getProduct = (req, res) =>{
	console.log(req.params.productId);

	return Product.findById(req.params.productId, {__v:0, createdOn: 0}).then(result => res.send(result)).catch(error =>res.send(false));
}


// Update Product information (Admin Only)
module.exports.updateProduct = (req, res) =>{
	const productData = auth.decode(req.headers.authorization);

	if(productData.isAdmin == true){
		let updateProduct = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
		}

		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new:true})
		.then(result =>{
			console.log(result);
			res.send(result);
		})
		.catch(error =>{
			console.log(error);
			res.send(false);
		});
	}
	else{
		return res.status(404).send("Ianccessible page, admin permission needed!");
	}
}


// Archive Product (Admin Only)
module.exports.archiveProduct = (req, res) =>{

	const productData = auth.decode(req.headers.authorization);

	let updateIsActiveField = {
		isActive: req.body.isActive
	}

	if(productData.isAdmin == true){
		return Product.findByIdAndUpdate(req.params.productId, updateIsActiveField)
		.then(result => {
			console.log(result);
			res.send(true);
		})
		.catch(error =>{
			console.log(error);
			res.send(false);
		})
	}
	else{
		return res.status(404).send("Ianccessible page, admin permission needed!");
	}
}





