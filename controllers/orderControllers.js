const mongoose = require("mongoose");
const User = require("../models/user.js");
const Product = require("../models/product");
const Order = require("../models/order.js");
const auth = require("../auth");



module.exports.addOrder = async (req, res) =>{

	const orderData = auth.decode(req.headers.authorization);

	if(orderData.isAdmin == false){
	let newOrder = await new Order({
		userId: orderData.id,
		email: orderData.email,
		productId: {},
		quantity: req.body.quantity,
		totalAmount: {}
	});


		if(newOrder.quantity > 0){
			let productData = await Product.findById(req.params.productId).then(product =>{
			
				product.stocks = product.stocks - newOrder.quantity
				
				if(product.stocks < 0){
					console.log("Item is out of stock!");
					return res.send("Item is out of stock!");

				}
				else{
					newOrder.productId = (req.params.productId)
					newOrder.totalAmount = product.price * newOrder.quantity
					console.log(newOrder);
					newOrder.save()
					return product.save()
				}

			})
			.then(result =>{
				res.send("Your order has been placed!")
				return console.log(result)
			})
			.catch(error =>{
			console.log(error)
			res.send("404 error")
			})
		}
		else{
			return res.send("Please input order quantity.")
		}
	}

	else{
		return res.send("Check your validation!")
	}

};









/*module.exports.addOrder = async (req, res) =>{

	const orderData = auth.decode(req.headers.authorization);

	let newOrder = await new Order({
		quantity: req.body.quantity
	});

	if(orderData.isAdmin == false){
		let productData = await Product.findById(req.params.productId).then(product =>{
			product.stocks = product.stocks - newOrder.quantity

			return product.save()
		})
		.then(result =>{
			res.send("Your order has been placed!")
			return console.log(result)
		})
		.catch(error =>{
		console.log(error)
		res.send("404 error")
		})
	}
	else{
		res.send("Check your validation!")
	}

	console.log(newOrder);
	return newOrder.save()
};
*/

/*module.exports.addOrder = async (req, res) =>{

	const orderData = auth.decode(req.headers.authorization);

	let newOrder = await new Order({
		userId: orderData.id,
		email: orderData.email,
		quantity: req.body.quantity,
		totalAmount: {}
	});


	if(orderData.isAdmin == false){
		let productData = await Product.findById(req.params.productId).then(product =>{
			product.stocks = product.stocks - newOrder.quantity
			newOrder.totalAmount = product.price * newOrder.quantity

			return product.save()
		})
		.then(result =>{
			res.send("Your order has been placed!")
			return console.log(result)
		})
		.catch(error =>{
		console.log(error)
		res.send("404 error")
		})
	}
	else{
		res.send("Check your validation!")
	}

	console.log(newOrder);
	return newOrder.save()
};*/


/*module.exports.addOrder = async (req, res) =>{

	const orderData = auth.decode(req.headers.authorization);

	if(orderData.isAdmin == false){
	let newOrder = await new Order({
		userId: orderData.id,
		email: orderData.email,
		quantity: req.body.quantity,
		totalAmount: {}
	});


		if(newOrder.quantity >= 1){
			let productData = await Product.findById(req.params.productId).then(product =>{
				product.stocks = product.stocks - newOrder.quantity
				newOrder.totalAmount = product.price * newOrder.quantity

				console.log(newOrder);
				newOrder.save()
				return product.save()
			})
			.then(result =>{
				res.send("Your order has been placed!")
				return console.log(result)
			})
			.catch(error =>{
			console.log(error)
			res.send("404 error")
			})
		}
		else{
			res.send("Please input order quantity.")
		}
		
	}

	else{
		res.send("Check your validation!")
	}

};*/
	
/*module.exports.addOrder = async (req, res) =>{

	const orderData = auth.decode(req.headers.authorization);

	if(orderData.isAdmin == false){
	let newOrder = await new Order({
		userId: orderData.id,
		email: orderData.email,
		productId: {},
		quantity: req.body.quantity,
		totalAmount: {}
	});


		if(newOrder.quantity > 0){
			let productData = await Product.findById(req.params.productId).then(product =>{
			
				product.stocks = product.stocks - newOrder.quantity
				
				if(product.stocks < 0){
					console.log("Item is out of stock!");
					return res.send("Item is out of stock!");

				}
				else{
					newOrder.productId = (req.params.productId)
					newOrder.totalAmount = product.price * newOrder.quantity
					console.log(newOrder);
					newOrder.save()
					return product.save()
				}

			})
			.then(result =>{
				res.send("Your order has been placed!")
				return console.log(result)
			})
			.catch(error =>{
			console.log(error)
			res.send("404 error")
			})
		}
		else{
			return res.send("Please input order quantity.")
		}
	}

	else{
		return res.send("Check your validation!")
	}

};*/




// [STRETCH GOAL]
/*module.exports.addToCart = (req, res) =>{

	const orderData = auth.decode(req.headers.authorization);


	let productData = Product.findById(req.body.productId).then(product => {
		
		let newQuantity = req.body.productId.orders.quantity

		console.log(product)
	})

}*/







































